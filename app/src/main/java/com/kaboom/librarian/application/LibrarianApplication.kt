package com.kaboom.librarian.application

import android.app.Application
import com.kaboom.librarian.di.MainModule
import com.kaboom.librarian.di.NetworkModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class LibrarianApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@LibrarianApplication)
            modules(MainModule, NetworkModule)
        }
    }
}