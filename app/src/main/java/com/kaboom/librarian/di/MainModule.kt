package com.kaboom.librarian.di

import com.kaboom.librarian.model.service.SearchProvider
import com.kaboom.librarian.model.service.implementation.SearchProviderImpl
import com.kaboom.librarian.ui.MainActivityViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

val MainModule: Module = module {
    viewModel { MainActivityViewModel(get()) }
    factory<SearchProvider> { SearchProviderImpl(get()) }
}