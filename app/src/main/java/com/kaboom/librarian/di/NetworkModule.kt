package com.kaboom.librarian.di

import com.kaboom.librarian.model.service.implementation.SearchBooksAPI
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

private const val SERVICE_BASE_URL = "http://openlibrary.org"

val NetworkModule = module {
    factory<SearchBooksAPI> {
        val retrofit = Retrofit.Builder()
            .baseUrl(SERVICE_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build() ?: throw Exception("Cannot create Retrofit service for URL $SERVICE_BASE_URL")

        retrofit.create(SearchBooksAPI::class.java)
    }
}