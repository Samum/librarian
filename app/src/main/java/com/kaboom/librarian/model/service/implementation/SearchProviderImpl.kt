package com.kaboom.librarian.model.service.implementation

import com.kaboom.librarian.model.service.SearchProvider
import com.kaboom.librarian.model.service.dto.OlResponse
import retrofit2.Response

class SearchProviderImpl(private val searchApi: SearchBooksAPI) : SearchProvider {
    override suspend fun searchBooks(searchString: String): Response<OlResponse> =
        searchApi.detectLanguage(searchString.replace(' ', '+', true))
}