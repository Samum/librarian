package com.kaboom.librarian.model.service

import com.kaboom.librarian.model.service.dto.OlResponse
import retrofit2.Response

interface SearchProvider {
    suspend fun searchBooks(searchString: String): Response<OlResponse>
}