package com.kaboom.librarian.model.service.implementation

import com.kaboom.librarian.model.service.dto.OlResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface SearchBooksAPI {
    @GET("/search.json")
    suspend fun detectLanguage(
        @Query("q") URLEncodedSource: String
    ): Response<OlResponse>
}