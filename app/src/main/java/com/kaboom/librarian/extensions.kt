package com.kaboom.librarian

import android.annotation.SuppressLint
import android.view.MotionEvent
import androidx.constraintlayout.widget.ConstraintLayout
import com.google.android.material.textview.MaterialTextView
import java.text.SimpleDateFormat
import java.util.*

@SuppressLint("ClickableViewAccessibility")
fun MaterialTextView.setClickAnimated() {
    this.setOnTouchListener { v, event ->
        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                val params = v.layoutParams as ConstraintLayout.LayoutParams
                params.width -= 5
                v.layoutParams = params
                v.elevation = 0F
                v.invalidate()
            }
            MotionEvent.ACTION_UP -> {
                val params = v.layoutParams as ConstraintLayout.LayoutParams
                params.width += 5
                v.layoutParams = params
                v.elevation = context.resources.getDimension(R.dimen.item_elevation)

                v.invalidate()
            }
        }
        false
    }
}

fun Long.toFormattedDate(format: String = "dd MMMM yyyy HH:mm:ss"): String {
    val formatter = SimpleDateFormat(format, Locale.getDefault())
    val calendar: Calendar = Calendar.getInstance()
    calendar.timeInMillis = this
    return formatter.format(calendar.time)
}

