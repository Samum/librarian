package com.kaboom.librarian.ui.fragments.results

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.kaboom.librarian.databinding.ResultsListItemBinding
import com.kaboom.librarian.model.service.dto.OlBookInfo


class ResultsListAdapter(private val items: List<OlBookInfo>) :
    RecyclerView.Adapter<ResultsListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(
            ResultsListItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items[position])

    override fun getItemCount(): Int = items.size

    inner class ViewHolder(private val binding: ResultsListItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: OlBookInfo) {
            binding.textViewTitle.text = item.title
            binding.textViewAuthor.text = item.author_name?.takeIf { it.isNotEmpty() }?.get(0)
            Glide.with(binding.root.context)
                .load("http://covers.openlibrary.org/b/id/${item.cover_i}-S.jpg")
                .into(binding.imageViewCover)
        }
    }
}
