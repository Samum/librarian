package com.kaboom.librarian.ui.fragments.results

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.kaboom.librarian.R
import com.kaboom.librarian.databinding.FragmentResultsBinding
import com.kaboom.librarian.setClickAnimated
import com.kaboom.librarian.ui.MainActivity
import com.kaboom.librarian.ui.MainActivityViewModel
import org.koin.androidx.viewmodel.ext.android.sharedViewModel


class ResultsFragment : Fragment() {
    private val viewModel: MainActivityViewModel by sharedViewModel()
    private lateinit var binding: FragmentResultsBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentResultsBinding.inflate(inflater, container, false).apply {
            with(buttonBack) {
                setClickAnimated()
                setOnClickListener(::onButtonBackClick)
            }
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.recyclerViewResults.layoutManager = LinearLayoutManager(view.context)
        binding.recyclerViewResults.adapter = ResultsListAdapter(emptyList())

        viewModel.searchResults.observe(viewLifecycleOwner) { response ->
            binding.recyclerViewResults.adapter = ResultsListAdapter(response.docs ?: emptyList())
        }
    }

    private fun onButtonBackClick(button: View) {
        (activity as? MainActivity)?.navigate(R.id.action_resultsFragment_to_searchFragment)
    }
}