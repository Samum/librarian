package com.kaboom.librarian.ui.fragments.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import com.kaboom.librarian.R
import com.kaboom.librarian.databinding.FragmentSearchBinding
import com.kaboom.librarian.setClickAnimated
import com.kaboom.librarian.ui.MainActivityViewModel
import com.pranavpandey.android.dynamic.toasts.DynamicToast
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class SearchFragment : Fragment() {
    private val viewModel: MainActivityViewModel by sharedViewModel()
    private lateinit var binding: FragmentSearchBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSearchBinding.inflate(inflater, container, false).apply {
            with(buttonSearch) {
                setClickAnimated()
                setOnClickListener(::onButtonSearchClick)
            }
        }
        return binding.root
    }

    private fun onButtonSearchClick(button: View) {

        val source = binding.editTextSearchString.text?.toString() ?: return

        if (source.isEmpty()) {
            DynamicToast.makeError(button.context, getString(R.string.empty_string_error)).show()
        } else {
            viewModel.searchBooks(source)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initObservers()
    }

    private fun initObservers() {
        viewModel.showProgress.observe(viewLifecycleOwner) { showProgress ->
            with(binding) {
                progressBar.isVisible = showProgress
                buttonSearch.isEnabled = !showProgress
                buttonSearch.text = if (showProgress) "" else getString(R.string.find_books)
            }
        }
    }
}