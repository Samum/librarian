package com.kaboom.librarian.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.kaboom.librarian.model.service.SearchProvider
import com.kaboom.librarian.model.service.dto.OlResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import okhttp3.ResponseBody

class MainActivityViewModel(private val searchProvider: SearchProvider) : ViewModel() {
    private val _showProgress = MutableLiveData<Boolean>()
    val showProgress: LiveData<Boolean> = _showProgress

    private val _searchResults = MutableLiveData<OlResponse>()
    val searchResults: LiveData<OlResponse> = _searchResults

    fun searchBooks(searchString: String) {
        _showProgress.postValue(true)

        viewModelScope.launch(Dispatchers.IO) {
            val response = searchProvider.searchBooks(searchString)
            _showProgress.postValue(false)
            if (response.isSuccessful) searchResponseReceived(response.body()) else searchErrorReceived(
                response.errorBody()
            )
        }
    }

    private fun searchResponseReceived(result: OlResponse?) {
        result?.let {
            _searchResults.postValue(it)
        }
    }

    private fun searchErrorReceived(error: ResponseBody?) {
        error?.let {
            //TODO: Process errors here
        }
    }
}