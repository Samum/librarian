package com.kaboom.librarian.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.kaboom.librarian.R
import com.kaboom.librarian.databinding.ActivityMainBinding
import com.pranavpandey.android.dynamic.toasts.DynamicToast
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {
    private val viewModel: MainActivityViewModel by viewModel()
    private lateinit var binding: ActivityMainBinding
    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater, null, false)
        setContentView(binding.root)
        navController =
            (supportFragmentManager.findFragmentById(R.id.navigation_host_fragment) as? NavHostFragment)?.navController
                ?: throw Exception(getString(R.string.no_navcontroller))

        viewModel.searchResults.observe(this) { response ->
            if (response.docs?.isNotEmpty() == true) {
                navigate(R.id.action_searchFragment_to_resultsFragment)
            } else {
                DynamicToast.makeError(this, getString(R.string.nothing_found)).show()
            }
        }
    }

    fun navigate(destId: Int) {
        if (this::navController.isInitialized) {
            navController.navigate(destId)
        }
    }
}